# Funktion für die Visualisierung von Häufigkeitsverteilungen als vertikale Tabellen
def Tabelle(x, y, Label) -> None:
    """Ausgabe von Häufigkeiten 'y' auf Positionen 'x'."""
    # Ausgabe des Tabellenkopfes
    print('{}:\t {}'.format(Label[0], Label[1]))
    # For-loop für jedes abgezählte Element.
    for xi, yi in zip(x,y):
        print('{:.2f}:\t\t {}'.format(xi, yi))

        
# Laden der Daten
# Laden von Bibliotheken
import pandas as pd
data = pd.read_csv('Rhodius2010.csv')
# Die ersten fünf Zeilen der Variable werden Ausgegeben.
print(data.head())


# Berechnen der Hamming Distanz
# Laden von Bibliotheken
import numpy as np # allgemeine mathematische Operationen
from itertools import combinations # erstellt Kombinationen aller Sequenzen
from scipy.spatial.distance import hamming # berechnet die Hamming Distanz

print('~~~~~~~~~~~~~~~~~~~~~~~~~~')
print('Hamming Distanz Berechnung')
print('~~~~~~~~~~~~~~~~~~~~~~~~~~')
# Extrahieren der Sequenzdaten als separate Liste
Sequenz = data['Sequence']
# Erzeugen aller paarweisen Sequenz-Permutationen
Kombinationen = combinations(Sequenz,2)
# Vordefinition einer Liste die für jede paarweise Sequenzkombination die Hamming Distanz speichert.
HammingDist = list()
# for loop über alle paarweisen Sequenzkombinationen.
# Combi ist eine Liste von zwei Strings die jeweils einer Sequenz entsprechen.
for Kombi in Kombinationen:
    # Konversion von Sequenz-string zu Sequenz-liste
    Seq1 = [base for base in Kombi[0]] 
    Seq2 = [base for base in Kombi[1]]
    # Anhängen der Hamming Dinstanz an die letzte Position
    HammingDist.append(hamming(Seq1, Seq2))

# Ordinalklassifizierung der Hamming Distanz zum erzeugen eines Säulendiagramms. 
# Abzählen der Hamming Distanz in Klassen.
Anzahl, DistanzKlasse = np.histogram(HammingDist)
Tabelle(DistanzKlasse, Anzahl, ['Distanz','Anzahl'])

# Berechnen der Entropie
print('~~~~~~~~~~~~~~~~')
print('Entropie Analyse')
print('~~~~~~~~~~~~~~~~')
from scipy.stats import entropy
PosZeilen = np.array([list(Seq) for Seq in Sequenz]).T
Entropie = list()
for Position in PosZeilen:
        _, counts = np.unique(Position, return_counts=True)
        Entropie.append(entropy(counts, base=4))
Tabelle(range(len(Entropie)), Entropie,['Position','Entropie'])